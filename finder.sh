#!/bin/bash

NEWLOC=`curl -L -I "https://mothersruin.com/software/Apparency/relnotes.html" -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/536.26.17 (KHTML, like Gecko) Version/6.0.2 Safari/536.26.17' 2>/dev/null | grep last-modified`

if [ "x${NEWLOC}" != "x" ]; then
	echo "${NEWLOC}"
fi
