#!/bin/bash -ex

# CONFIG
prefix="Apparency"
suffix=""
munki_package_name="Apparency"
display_name="Apparency"
icon_name=""
url="https://www.mothersruin.com/software/downloads/Apparency.dmg"

# download it (-L: follow redirects)
curl -L -o app.dmg -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.1 Safari/537.36' "${url}"

## EXAMPLE: unpacking a flat package to find appropriate things
## Mount disk image on temp space
# mountpoint=`hdiutil attach -mountrandom /tmp -nobrowse app.dmg | awk '/private\/tmp/ { print $3 } '`

#/usr/local/bin/unar app.xip
## Unpack
#/usr/sbin/pkgutil --expand app.pkg pkg
#mkdir build-root
#(cd build-root; pax -rz -f ../pkg/*/Payload)
# hdiutil detach "${mountpoint}"

#hdiutil create -srcfolder app -format UDZO -o app.dmg

## Find all the appropriate apps, etc, and then turn that into -f's
#key_files=`find build-root -name '*.qlgenerator' -maxdepth 3 | sed 's/ /\\\\ /g; s/^/-f /' | paste -s -d ' ' -`

## Build pkginfo (this is done through an echo to expand key_files)
#echo /usr/local/munki/makepkginfo -m go-w -g admin -o root app.dmg ${key_files} | /bin/bash > app.plist

## Fixup and remove "build-root" from file paths
#perl -p -i -e 's/build-root//' app.plist
## END EXAMPLE

# Build pkginfo
/usr/local/munki/makepkginfo app.dmg > app.plist

pwd=`pwd`
plist=`pwd`/app.plist

# Obtain version info
version=`defaults read "${plist}" version`

minver=`/usr/libexec/PlistBuddy -c "Print :installs:0:minosversion" "${plist}"`

# For silent packages, configure blocking_applications and unattended_install
# defaults write "${plist}" blocking_applications -array "VirtualBox.app" "/Path/To/Executable" "iTunesHelper"
# defaults write "${plist}" unattended_install -bool YES

# Change path and other details in the plist
defaults write "${plist}" installer_item_location "izzyteam/${prefix}-${version}${suffix}.dmg"
defaults write "${plist}" minimum_os_version "${minver}"
defaults write "${plist}" uninstallable -bool NO
defaults write "${plist}" name "${munki_package_name}"
defaults write "${plist}" display_name "${display_name}"
defaults write "${plist}" icon_name "${icon_name}"
defaults write "${plist}" version "${version}"

# Obtain display name from Izzy and add to plist
displayname=`/usr/local/bin/displaynametool "${munki_package_name}"`
defaults write "${plist}" display_name -string "${displayname}"

# Obtain description from Izzy and add to plist
description=`/usr/local/bin/descriptiontool "${munki_package_name}"`
defaults write "${plist}" description -string "${description}"

# Obtain category from Izzy and add to plist
category=`/usr/local/bin/cattool "${munki_package_name}"`
defaults write "${plist}" category "${category}"

# Make readable by humans
/usr/bin/plutil -convert xml1 "${plist}"
chmod a+r "${plist}"

# Change filenames to suit
mv app.dmg   ${prefix}-${version}${suffix}.dmg
mv app.plist ${prefix}-${version}${suffix}.plist
